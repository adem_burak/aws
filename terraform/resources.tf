resource "aws_instance" "tfer--i-002D-0181e5c9a1c980fcb_" {
  ami                         = "ami-035966e8adab4aaad"
  associate_public_ip_address = "true"
  availability_zone           = "eu-west-1c"
  cpu_core_count              = "1"
  cpu_threads_per_core        = "1"
  credit_specification {
    cpu_credits = "standard"
  }
  disable_api_termination = "false"
  ebs_optimized           = "false"
  get_password_data       = "false"
  instance_type           = "t2.micro"
  ipv6_address_count      = "0"
  key_name                = "burakaws"
  monitoring              = "false"
  private_ip              = "172.31.28.92"
  root_block_device {
    delete_on_termination = "true"
    encrypted             = "false"
    iops                  = "100"
    volume_size           = "8"
    volume_type           = "gp2"
  }
  security_groups = [
    "launch-wizard-1",
  ]
  source_dest_check = "true"
  subnet_id         = "subnet-4c162204"
  tags = {
    resource_group = "test_resources"
    burakadded     = "myvalue"
    testproperty   = "test property value"
  }
  tenancy = "default"
  vpc_security_group_ids = [
    "sg-0198c718d567b3345",
  ]
}
